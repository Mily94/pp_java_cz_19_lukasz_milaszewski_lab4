package lab4;

public class PlayerThread implements Runnable {
	
	Object obj;
	Player player;
	private GUI gui;
	
	public PlayerThread(Object obj, Player player, GUI gui) {
		this.obj = obj;
		this.player = player;
		this.gui = gui;
	}
	
	@Override
	public void run() {
		while(true) {
			synchronized (obj) {
				int height = gui.getHeight();
				int width = gui.getWidth();
				if(height > 0){
					Game.performPaddleMove(player, height);	
					Game.itemVisibility(width, height);
					//Game.itemGained(player);
				}
				try {
					obj.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}	
