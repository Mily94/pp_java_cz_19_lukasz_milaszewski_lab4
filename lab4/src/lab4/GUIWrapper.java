package lab4;

import java.awt.BorderLayout;
import javax.swing.JFrame;

public class GUIWrapper {
    
    public GUIWrapper(Player player1, Player player2, Ball ball) {
    	JFrame.setDefaultLookAndFeelDecorated(true);
    	JFrame frame = new JFrame("Pong");
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	frame.setLayout(new BorderLayout());
    	frame.setResizable(false);
    	 	
    	GUI gui = new GUI(player1, player2, ball);
    	
    	frame.add(gui, BorderLayout.CENTER);
    		
    	frame.setSize(1010, 633);
    	frame.setVisible(true);	
    }
    
}
